import { writeFileSync, readFileSync, existsSync, mkdirSync } from 'fs'
import path from 'path'

function ensureDirectoryExistence(filePath) {
	var dirname = path.dirname(filePath);
	if (existsSync(dirname)) {
		return true;
	}
	ensureDirectoryExistence(dirname);
	mkdirSync(dirname);
}

export interface Mood {
	emoji: string,
	name: string,
	links: string[]
}

const saveFileName = 'data/moods.json';
export function saveMoods() {
	ensureDirectoryExistence(saveFileName);
	writeFileSync(saveFileName, JSON.stringify(moods));
}

export function loadMoods() {
	if (existsSync(saveFileName)) {
		var moodString = readFileSync(saveFileName, { encoding: 'UTF-8' });
		moods = JSON.parse(moodString);
	}
	else {
		saveMoods();
	}
}

export var moods: Mood[] = [
	{
		emoji: '😏',
		name: 'Troll',
		links: [
			'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
		]
	},
	{
		emoji: '🔥',
		name: 'Fire',
		links: [
			'https://www.youtube.com/watch?v=bmlg1HkjUjA',
			'https://www.youtube.com/watch?v=DPbvoJlBRkw',
			'https://www.youtube.com/watch?v=JJ9rEcqxM2E'
		]
	},
];



