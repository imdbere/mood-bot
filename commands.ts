import { client } from "./index";
import { saveMoods, Mood, loadMoods, moods } from "./mood";
import Discord, { Message, StreamDispatcher } from 'discord.js'
import ytdl from 'ytdl-core'
import ytpl from 'ytpl'
import { registerCommandHandler, registerCommandAlias } from "./commandLib";

loadMoods();

interface PlayingState {
	mood: Mood,
	playingIndex: number,
	dispatcher: StreamDispatcher,
	nowPlayingMessage: Discord.Message
}

var playingList = new Map<string, PlayingState>();

function isPlaylist(link: string) {
	var isValid = (ytpl as any).validateURL(link);
	return isValid;
}

async function resolvePlaylist(link: string) {
	var playlist = await ytpl(link);
	return playlist;
	//return playlist.items.map(i => i.url_simple);
}

function playVideo(link: string, connection: Discord.VoiceConnection) {
	var videoStream = ytdl(link, { filter: 'audioonly' });
	return connection.play(videoStream);
}

function shuffle(a) {
	var j, x, i;
	for (i = a.length - 1; i > 0; i--) {
		j = Math.floor(Math.random() * (i + 1));
		x = a[i];
		a[i] = a[j];
		a[j] = x;
	}
	return a;
}

process.on('SIGINT', closeGracefully);
process.on('SIGTERM', closeGracefully);

async function closeGracefully() {
	try {
		var playingChannels = playingList.values();
		for (const playing of playingChannels) {
			await playing.nowPlayingMessage.reply('Bot is restarting...');
		}
	}
	catch (e) {
		console.log(e);
		process.exit(1);
	}

	process.exit(0);
}


export async function startPlaying(msg: Message, voice: Discord.VoiceState, mood: Mood) {
	var playing = playingList.get(voice.id);
	if (playing && playing.nowPlayingMessage) {
		// Already playing
		playing.nowPlayingMessage.delete();
	}
	var connection = voice.connection;
	if (msg) {
		msg.reply('Now playing somethin of the mood ' + mood.name);
	}

	shuffle(mood.links);
	var videoLink = mood.links[0];
	var dispatcher = playVideo(videoLink, connection);

	var nowPlayingMessage = msg ? await msg.reply('Now playing: ' + videoLink) : null;

	var playing: PlayingState = { mood: mood, dispatcher, playingIndex: 0, nowPlayingMessage };
	playingList.set(voice.id, playing);

	dispatcher.on('finish', () => {
		playNext(voice, playing);
		//playing.nowPlayingMessage.delete()
	});
}

async function playNext(voice: Discord.VoiceState, playing: PlayingState) {
	var videoLinks = playing.mood.links;
	var newPlayingIndex = playing.playingIndex + 1;
	if (newPlayingIndex >= videoLinks.length) {
		newPlayingIndex = 0;
	}

	var link = videoLinks[newPlayingIndex];

	var nowPlayingMessage
	if (playing.nowPlayingMessage) {
		var channel = playing.nowPlayingMessage.channel;
		await playing.nowPlayingMessage.delete();
		nowPlayingMessage = await channel.send('Now playing: ' + link);
	}


	var dispatcher = playVideo(link, voice.connection);
	dispatcher.on('finish', () => {
		playNext(voice, playing);
		//playing.nowPlayingMessage.delete();
	});

	playing.playingIndex = newPlayingIndex;
	playing.dispatcher = dispatcher;
	playing.nowPlayingMessage = nowPlayingMessage;
	return link;
}

async function reactWithMoodChoice(msg: Message) {
	var allMoodEmojis = moods
		.map(v => v.emoji)
		.map(e => msg.react(e));

	await Promise.all(allMoodEmojis);

	var collector = msg.createReactionCollector((reaction, user) => {
		var validEmojis = moods.map(m => m.emoji);
		return validEmojis.includes(reaction.emoji.name) && user.id != client.user.id;
	}, { time: 20000 });

	collector.on('collect', reaction => {
		var voice = reaction.message.guild.voice;
		var moodForEmoji = moods.find(v => v.emoji == reaction.emoji.name);
		startPlaying(msg, voice, moodForEmoji);

		msg.delete();
	});
}

export function searchMood(text: string) {
	// search by emoji
	var mood = moods.find(v => v.emoji == text);
	if (!mood) {
		// search by name
		mood = moods.find(v => v.name.toUpperCase() == text.toUpperCase());
	}

	return mood;
}

export function registerCommands() {

	registerCommandHandler('addvideo', ['mood', 'link'], async (params, msg: Message) => {
		var valid = /http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/.test(params.link);
		if (valid) {
			// search by emoji
			var mood = searchMood(params.mood);
			if (mood) {

				if (isPlaylist(params.link)) {
					var pl = await resolvePlaylist(params.link);
					var text = 'This is a playlist with ' + pl.total_items + ' items, title: ' + pl.title + '\n';
					text += 'Adding following titles: \n' + pl.items.map(i => i.title).join(', \n');
					msg.reply(text);

					var items = pl.items.map(i => i.url_simple);
					mood.links.push(...items);
				}
				else {
					mood.links.push(params.link);
					msg.reply('Your link has been added to the ' + mood.name + ' mood');
				}

				saveMoods();
			}
			else {
				msg.reply('The mood ' + params.mood + ' doesnt exist, please use !addmood to create it first');
			}
		}
		else {
			msg.reply('Invalid link');
		}
	});

	registerCommandHandler('addmood', ['emoji', 'name'], (params, msg) => {
		var emojiRegex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/;
		var emoji = params.emoji;
		var name = params.name;

		if (!emojiRegex.test(emoji)) {
			// Try reverse
			name = params.emoji;
			emoji = params.name;
		}

		if (!emojiRegex.test(emoji)) {
			msg.reply('No emoji found in the command, use !addmod <emoji> <mood_name>');
			return;
		}

		var existing = moods.find(v => v.emoji == emoji || v.name == name);

		if (!existing) {
			moods.push({
				emoji: emoji,
				name: name,
				links: []
			});
			msg.reply('Mood created!');
			saveMoods();
		}
		else {
			msg.reply('This mood already exists')
		}
	});

	registerCommandHandler('rmmood', ['mood'], (params, msg) => {
		var mood = moods.find(m => m.name == params.mood);
		var index = moods.indexOf(mood);

		moods.splice(index, 1);
		msg.reply(`Mood ${mood.name} (${mood.emoji}) removed!`);
	});

	registerCommandHandler('rmvid', [], (params, msg) => {
		// Remove currently playing video
		var voice = msg.guild.voice;
		if (voice) {
			var playing = playingList.get(voice.id);
			if (playing) {
				var elementToDelete = playing.mood.links[playing.playingIndex];
				playing.mood.links.splice(playing.playingIndex, 1);
				msg.reply('Removed video with following link from mood ' + playing.mood.emoji + ': ' + elementToDelete);

				saveMoods();
				return;
			}
		}

		msg.reply('Nothing to remove, no currently playing video');
	});

	registerCommandHandler('lsmoods', [], (params, msg) => {
		var text = "Moods: \n";
		for (const mood of moods) {
			text += `${mood.name} (${mood.emoji}) \n`;
		}

		msg.reply(text);
	});

	registerCommandHandler('join', [], async (params, msg) => {
		if (msg.member.voice.channel) {
			const connection = await msg.member.voice.channel.join();
			var newMsg = await msg.reply('Great! now what do you want to hear ?');
			//reactWithMoodChoice(newMsg);
		} else {
			msg.reply('You need to join a voice channel first!');
		}
	});

	registerCommandHandler('leave', [], async (params, msg: Message) => {
		if (msg.guild.voice?.connection) {
			var playing = playingList.get(msg.guild.voice.id);
			if (playing && playing.nowPlayingMessage) {
				playing.nowPlayingMessage.delete();
			}

			msg.guild.voice.connection.disconnect();
		}
	});

	registerCommandHandler('pause', [], async (params, msg: Message) => {
		var voice = msg.guild.voice;
		if (voice) {
			var playing = playingList.get(voice.id);
			if (playing) {
				playing.dispatcher.pause();
				return;
			}
		}
		msg.reply('Nothing to stop');
	});

	registerCommandAlias('pause', 'stop');
	registerCommandAlias('pause', 'stfu');

	registerCommandHandler('resume', [], async (params, msg: Message) => {
		var voice = msg.guild.voice;
		if (voice) {
			var playing = playingList.get(voice.id);
			if (playing) {
				playing.dispatcher.resume();
				return;
			}
		}
		msg.reply('Nothing to resume');
	});

	registerCommandHandler('play', [], async (params, msg, optionalParams) => {
		if (msg.guild.voice && msg.guild.voice.connection) {
			if (optionalParams) {
				var moodName = optionalParams[0];
				var mood = searchMood(moodName);
				if (mood) {
					startPlaying(msg, msg.guild.voice, mood);
				}
				else {
					msg.reply('No mood with this name found');
				}
			}
			else {
				var newMsg = await msg.reply('What do you want to hear ?');
				//reactWithMoodChoice(newMsg);
			}
		}
		else {
			msg.reply('Please use !join to let me join a voice channel first');
		}
	});

	registerCommandHandler('next', [], async (params, msg) => {
		if (msg.guild.voice) {
			var voiceChannelId = msg.guild.voice.id;
			var playing = playingList.get(voiceChannelId);
			if (playing) {
				var videoLink = playNext(msg.guild.voice, playing);
				return;
			}
		}

		msg.reply('You first have to use !play to select a mood');
	});
}

