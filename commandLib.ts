import { Message, MessageEmbed } from "discord.js";

export const commandPrefix = "/";

var commandList = [{
	command: 'ping',
	parameterNames: [],
	handler: (params, msg: Message, optionalParams) => msg.reply('!pong')
}];

type commandHandler = ((params, msg: Message) => any) | ((params, msg: Message, optionalParams) => any);

export function registerCommandHandler(command: string, parameterNames: string[], handler: commandHandler) {
	commandList.push({
		command,
		parameterNames,
		handler
	});
}

export function registerCommandAlias(command: string, alias: string) {
	var originalCommand = commandList.find(c => c.command == command);
	commandList.push({
		command: alias,
		parameterNames: originalCommand.parameterNames,
		handler: originalCommand.handler
	});
}

export function handleCommands(msg: Message) {
	var content = msg.content;
	var foundCommand = false;
	var split = content.split(' ');
	var readCommand = split[0];

	for (const command of commandList) {
		if (commandPrefix + command.command == readCommand) {
			var namedParametersLength = command.parameterNames.length
			if (split.length >= namedParametersLength + 1) {
				var rawParameters = split.slice(1).filter(p => p.length > 0);
				var parameters = rawParameters
					.slice(0, namedParametersLength)
					.map((val, i) => [command.parameterNames[i], val]);
				var parameterObj = Object.fromEntries(parameters);

				if (rawParameters.length > namedParametersLength) {
					var optionalParameters = rawParameters.slice(namedParametersLength);
					command.handler(parameterObj, msg, optionalParameters);
				}
				else {
					command.handler(parameterObj, msg, null);
				}

			}
			else {
				msg.reply(`You are not using the command ${readCommand} correctly, use it like this: `);
				msg.reply(readCommand + ' ' + command.parameterNames.join(' '));
			}

			msg.react('👌');
			foundCommand = true;
			break;
		}
	}

	if (!foundCommand) {
		msg.reply('No command found with name ' + readCommand);
	}
}


registerCommandHandler('help', [], (params, msg) => {
	var text = "";
	for (const command of commandList) {
		text += command.command + " ";
		text += command.parameterNames.map(p => '<' + p + '>').join(' ') + '\n'
	}

	const exampleEmbed = new MessageEmbed()
		.setColor('#0099ff')
		.setTitle('Mood-Bot help screen')
		.setDescription(text)

	msg.reply(exampleEmbed);
});