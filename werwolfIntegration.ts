import { Message, VoiceChannel, VoiceState, Snowflake } from "discord.js";
import { searchMood, startPlaying } from "./commands";

var channelModerators = new Map<Snowflake, Snowflake>();
var currentPhase = new Map<Snowflake, string>();

export async function handleWerwolfMessage(msg: Message) {

	if (msg.content.includes("newgame")) {
		channelModerators.set(msg.channel.id, msg.author.id);
	}

	if (msg.author.bot && msg.author.username == "WerwolfBot") {
		if (msg.embeds.length > 0) {
			var embed = msg.embeds[0];
			if (embed.title?.includes("New Game")) {
				msg.reply("Hey @WerwolfBot! lets do this game together, shall we?");
				msg.channel.send("For optimal results, please create and populate the 'day', 'morning', 'night', 'lobby' and 'battle' moods");

				/*var voiceChannel: VoiceChannel;
				for (const message of msg.channel.messages.cache) {
					var voice = message[1].member.voice.channel;
					if (voice) {
						voiceChannel = voice;
						break;
					}
				}*/

				var moderatorId = channelModerators.get(msg.channel.id);
				var moderator = msg.guild.members.cache.get(moderatorId);
				

				if (!moderator.voice.channel) {
					msg.reply("I am sorry, but i cant find any voice channel to join");
					return;
				}

				await moderator.voice.channel.join();
				await playPhase("lobby", msg.guild.voice, msg.channel.id);

				// New Game (lobby)
			}

			if (embed.title?.includes("Willkommen bei : Die Werölfe von Düsterwald")) {
				// Game start (first night)
				await playPhase("night", msg.guild.voice, msg.channel.id);
			}

			if (embed.title?.includes("TAG")) {
				// Day
				await playPhase("day", msg.guild.voice, msg.channel.id);
			}

			if (embed.title?.includes("Auf dem Schafott steht")) {
				// Day
				await playPhase("battle", msg.guild.voice, msg.channel.id);
			}

			if (embed.title?.includes("NACHT")) {
				// Night
				await playPhase("night", msg.guild.voice, msg.channel.id);
			}

			if (embed.title?.includes("MORGEN")) {
				// Morning
				await playPhase("morning", msg.guild.voice, msg.channel.id);
			}

			if (embed.title?.includes("DIE DORFBEWOHNER GEWINNEN")) {
				// Game end, villagers win
				await playPhase("victory", msg.guild.voice, msg.channel.id);
			}
			if (embed.title?.includes("DIE WERWÖLFE GEWINNEN")) {
				// Game end, wervolves win
				await playPhase("defeat", msg.guild.voice, msg.channel.id);
			}

		}
	}
}

async function playPhase(phaseName, voice: VoiceState, channelId: Snowflake) {
	if (currentPhase.get(channelId) == phaseName) {
		return;
	}

	currentPhase.set(channelId, phaseName);
	var mood = searchMood(phaseName);
	if (mood) {
		await startPlaying(null, voice, mood);
	}
}